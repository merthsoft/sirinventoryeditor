﻿namespace SirInventoryEditor {
	partial class CountAdjuster {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.countBox = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.maxCountLabel = new System.Windows.Forms.Label();
			this.warningLabel = new System.Windows.Forms.Label();
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.countBox)).BeginInit();
			this.SuspendLayout();
			// 
			// countBox
			// 
			this.countBox.Location = new System.Drawing.Point(56, 10);
			this.countBox.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
			this.countBox.Name = "countBox";
			this.countBox.Size = new System.Drawing.Size(120, 20);
			this.countBox.TabIndex = 0;
			this.countBox.ValueChanged += new System.EventHandler(this.countBox_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Count:";
			// 
			// maxCountLabel
			// 
			this.maxCountLabel.AutoSize = true;
			this.maxCountLabel.Location = new System.Drawing.Point(182, 12);
			this.maxCountLabel.Name = "maxCountLabel";
			this.maxCountLabel.Size = new System.Drawing.Size(21, 13);
			this.maxCountLabel.TabIndex = 2;
			this.maxCountLabel.Text = "/ 1";
			// 
			// warningLabel
			// 
			this.warningLabel.AutoSize = true;
			this.warningLabel.ForeColor = System.Drawing.Color.Red;
			this.warningLabel.Location = new System.Drawing.Point(15, 38);
			this.warningLabel.Name = "warningLabel";
			this.warningLabel.Size = new System.Drawing.Size(176, 13);
			this.warningLabel.TabIndex = 3;
			this.warningLabel.Text = "Warning: Count is greater than max.";
			this.warningLabel.Visible = false;
			// 
			// okButton
			// 
			this.okButton.Location = new System.Drawing.Point(52, 54);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(75, 23);
			this.okButton.TabIndex = 4;
			this.okButton.Text = "Accept";
			this.okButton.UseVisualStyleBackColor = true;
			this.okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(133, 54);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 5;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// CountAdjuster
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(220, 87);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.warningLabel);
			this.Controls.Add(this.maxCountLabel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.countBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "CountAdjuster";
			this.Text = "Adjust Count";
			((System.ComponentModel.ISupportInitialize)(this.countBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.NumericUpDown countBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label maxCountLabel;
		private System.Windows.Forms.Label warningLabel;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
	}
}