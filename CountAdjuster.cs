﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace SirInventoryEditor {
	public partial class CountAdjuster : Form {
		private int max;
		public int Max {
			get {
				return max;
			}
			set {
				max = value;
				maxCountLabel.Text = string.Format("/{0}", max);
			}
		}

		public int Count {
			get {
				return (int)countBox.Value;
			}
			set {
				countBox.Value = value;
			}
		}

		public CountAdjuster() {
			InitializeComponent();
		}

		private void okButton_Click(object sender, EventArgs e) {
			this.DialogResult = DialogResult.OK;
			Close();
		}

		private void countBox_ValueChanged(object sender, EventArgs e) {
			warningLabel.Visible = Count > Max;
		}
	}
}
