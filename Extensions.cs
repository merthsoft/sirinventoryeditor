﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace SirInventoryEditor {
	public static class Extensions {
		public static bool IsNullOrWhiteSpace(string s) {
			return string.IsNullOrEmpty(s) || string.IsNullOrEmpty(s.Trim());
		}

		public static string SpaceBeforeCapitals(this string s) {
			if (Extensions.IsNullOrWhiteSpace(s) || s.Length < 2) { return s; }
			
			StringBuilder ret = new StringBuilder();
			ret.Append(s[0]);
			char prevChar = s[0];

			for (int i = 1; i < s.Length; i++) {
				char c = s[i];
				if (char.IsUpper(c) && prevChar != ' ') {
					ret.Append(' ');
				}
				ret.Append(c);
				prevChar = c;
			}

			return ret.ToString();
		}

		/// <summary>
		/// Adds a filter to the FileDialog.
		/// </summary>
		/// <param name="text">The text to display.</param>
		/// <param name="ext">The extensions.</param>
		public static void AddFilter(this FileDialog ofd, string text, params string[] ext) {
			StringBuilder compiledExt = new StringBuilder(6 * ext.Length);
			for (int i = 0; i < ext.Length; i++) {
				compiledExt.AppendFormat("{1}{0}", ext[i], i == 0 ? "" : ";");
			}
			ofd.Filter = string.Concat(ofd.Filter, ofd.Filter != "" ? "|" : "", text, "|", compiledExt.ToString());
		}
	}
}
