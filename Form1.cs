﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace SirInventoryEditor {
	public partial class Form1 : Form {
		string fileName = null;
		bool dirtyFlag = false;

		int tileSize = SirItem.TileSize;
		int invWidth = 6;
		int invHeight = 10;

		List<SirItem> items;
		Dictionary<string, SirItem> itemDictionary;

		List<InventoryItem> inventory = new List<InventoryItem>();

		public Form1() {
			InitializeComponent();

			var root = new XmlRootAttribute("SirItems") { Namespace = "http://merthsoft.com/SirItems" };
			var deserializer = new XmlSerializer(typeof(List<SirItem>), root);
			items = (List<SirItem>)deserializer.Deserialize(File.Open("SirItems.xml", FileMode.Open, FileAccess.Read)); ;
			items.Sort();

			itemDictionary = items.ToDictionary(si => si.Name);

			foreach (var item in items) {
				ListViewItem lvi = new ListViewItem(item.ToString()) { Tag = item, ImageKey = item.Name };
				lvi.ToolTipText = item.Description;
				itemImageList.Images.Add(item.Name, item.Thumbnail);
				itemList.Items.Add(lvi);
			}

			inventoryPanel.Width = tileSize * invWidth;
			inventoryPanel.Height = tileSize * invHeight;
		}

		private void itemList_MouseMove(object sender, MouseEventArgs e) {
			//int index = itemList.IndexFromPoint(e.Location);
			//if (index != -1) {
			//	string desc = items[index].Description;
			//	if (descriptionToolTip.GetToolTip(itemList) != desc) {
			//		descriptionToolTip.SetToolTip(itemList, desc);
			//	}
			//}
		}

		private void itemList_DragLeave(object sender, EventArgs e) {

		}

		private void inventoryPanel_DragEnter(object sender, DragEventArgs e) {
			e.Effect = DragDropEffects.All;
			itemDetailLabel.Visible = true;
			dragonDropBox.Visible = true;
			dragonDropBox.Enabled = true;
			var point = inventoryPanel.PointToClient(new Point(e.X, e.Y));
			dragonDropBox.Location = new Point(point.X / tileSize * tileSize, point.Y / tileSize * tileSize);
			SirItem sirItem = ((InventoryItem)e.Data.GetData(typeof(InventoryItem))).Item;
			dragonDropBox.Width = sirItem.Width * tileSize;
			dragonDropBox.Height = sirItem.Height * tileSize;
			dragonDropBox.Image = new Bitmap(sirItem.Width * tileSize, sirItem.Height * tileSize);
			using (Graphics g = Graphics.FromImage(dragonDropBox.Image)) {
				//g.FillRectangle(Brushes.Transparent, 0, 0, tileSize, tileSize);
				g.DrawImage(sirItem.Image, 0, 0);
			}
			inventoryPanel.BackgroundImage = new Bitmap(invWidth * tileSize, invHeight * tileSize);
			using (Graphics g = Graphics.FromImage(inventoryPanel.BackgroundImage)) {
				drawInventory(g);
			}
		}

		private void inventoryPanel_DragLeave(object sender, EventArgs e) {
			itemDetailLabel.Visible = false;
			dragonDropBox.Visible = false;
			dragonDropBox.Enabled = false;
			dragonDropBox.Location = new Point(-1000, -1000);
		}

		private void inventoryPanel_DragDrop(object sender, DragEventArgs e) {
			dragonDropBox.Visible = false;
			dragonDropBox.Enabled = false;
			dragonDropBox.Location = new Point(-1000, -1000);

			Point point = inventoryPanel.PointToClient(new Point(e.X, e.Y));
			InventoryItem newInventoryItem = (InventoryItem)e.Data.GetData(typeof(InventoryItem));
			newInventoryItem.X = point.X / tileSize;
			newInventoryItem.Y = point.Y / tileSize;
			inventory.Add(newInventoryItem);

			dirtyFlag = true;
			inventoryImage.Invalidate();
		}

		private void inventoryPanel_DragOver(object sender, DragEventArgs e) {
			var point = inventoryPanel.PointToClient(new Point(e.X, e.Y));
			itemDetailLabel.Visible = true;
			itemDetailLabel.Text = string.Format("({0}, {1})", point.X / tileSize, point.Y / tileSize);
			dragonDropBox.Location = new Point(point.X / tileSize * tileSize, point.Y / tileSize * tileSize);
		}

		private void itemList_MouseDown(object sender, MouseEventArgs e) {
		}

		private void itemList_MouseLeave(object sender, EventArgs e) {
			if (itemList.SelectedItems.Count > 0) {
				DoDragDrop(new InventoryItem((SirItem)itemList.SelectedItems[0].Tag), DragDropEffects.All);
			}
		}

		private void inventoryPanel_Paint(object sender, PaintEventArgs e) {
		}

		private void inventoryImage_Paint(object sender, PaintEventArgs e) {
			Graphics graphics = e.Graphics;
			drawInventory(graphics);
		}

		private void drawInventory(Graphics graphics) {
			graphics.FillRectangle(Brushes.DarkGreen, 0, 0, inventoryPanel.Width, inventoryPanel.Height);

			for (int i = 0; i < invWidth; i++) {
				for (int j = 0; j < invHeight; j++) {
					graphics.DrawImage(itemDictionary["NoWeapon"].Image, i * tileSize, j * tileSize, tileSize, tileSize);
				}
			}

			inventory.ForEach(i => graphics.DrawImage(i.Item.Image, i.X * tileSize, i.Y * tileSize));
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.AddFilter("Sir Inventory File", "PlayerInv.xml");
			ofd.AddFilter("All files", "*.*");
			ofd.CheckFileExists = true;
			ofd.Multiselect = false;
			if (ofd.ShowDialog() != System.Windows.Forms.DialogResult.OK) { return; }
			fileName = ofd.FileName;
			using (Stream s = ofd.OpenFile()) {
				openFile(s);
			}
		}

		private void openFile(Stream s) {
			inventory.Clear();
			XmlSerializer serializer = new XmlSerializer(typeof(InventoryList));
			InventoryList list = (InventoryList)serializer.Deserialize(s);

			for (int i = 0; i < list.PickupItemList.Count; i++) {
				InventoryItem newItem = null;
				string itemName = list.PickupItemList[i];
				if (itemName.StartsWith("Throw") || itemName == "NoWeapon") { continue; }
				if (!itemDictionary.ContainsKey(itemName)) {
					SirItem si = new SirItem() { Name = itemName, Height = 1, Width = 1, MaxCount = 1, Description = "Unknown item" };
					itemDictionary[itemName] = si;
				}

				newItem = new InventoryItem(itemDictionary[itemName], list.PickupPositionList[i]);
				newItem.Count = list.PickupItemValueList[i];

				inventory.Add(newItem);
			}

			inventoryImage.Invalidate();
		}

		private InventoryList createSerializableList(string name) {
			InventoryList ret = new InventoryList() { InventoryName = name };
			if (name == "PlayerInventory") {
				string[] dummyList = new[] { "NoWeapon", "ThrowBottles", "ThrowClocks", "ThrowDynamite", "ThrowStones", "ThrowTrains", "ThrowTraps" };
				foreach (string s in dummyList) {
					ret.PickupItemList.Add(s);
					ret.PickupItemValueList.Add(0);
					ret.PickupPositionList.Add(new InventoryPoint(0, 0));
				}
			}
			foreach (var item in inventory) {
				ret.PickupItemList.Add(item.Name);
				ret.PickupItemValueList.Add(item.Count);
				ret.PickupPositionList.Add(item.Location);
			}

			return ret;
		}

		private void setSaveFile() {
			SaveFileDialog sfd = new SaveFileDialog();
			sfd.AddFilter("Sir Inventory File", "PlayerInv.xml");
			sfd.AddFilter("All files", "*.*");
			if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK) { return; }
			fileName = sfd.FileName;
		}

		private bool saveFile() {
			return saveFile(fileName == null);
		}

		private bool saveFile(bool setSave) {
			if (setSave) {
				setSaveFile();
				if (fileName == null) {
					return false;
				}
			}

			using (Stream s = File.Open(fileName, FileMode.Create, FileAccess.Write)) {
				return saveFile(s);
			}
		}

		private bool saveFile(Stream s) {
			var serializer = new XmlSerializer(typeof(InventoryList));
			try {
				serializer.Serialize(s, createSerializableList("PlayerInventory"));
				dirtyFlag = false;
			} catch (Exception ex) {
				MessageBox.Show(string.Format("Unable to save file:{0}{1}", Environment.NewLine, ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			return true;
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
			saveFile();
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
			saveFile(true);
		}

		private void inventoryImage_MouseMove(object sender, MouseEventArgs e) {
			int x = e.X / tileSize;
			int y = e.Y / tileSize;
			itemDetailLabel.Text = string.Format("({0}, {1})", x, y);
			var item = getItemFromPoint(x, y);
			if (item != null) {
				itemDetailLabel.Text += string.Format(" - {0}: {3} ({1}/{2})", item.Name.SpaceBeforeCapitals(), item.Count, item.Item.MaxCount, item.Item.Description);
			}
		}

		private void inventoryImage_MouseDown(object sender, MouseEventArgs e) {
			InventoryItem hoverItem = getItemFromPoint(e.X / tileSize, e.Y / tileSize);
			if (e.Button == System.Windows.Forms.MouseButtons.Left && hoverItem != null) {
				if ((Control.ModifierKeys & Keys.Control) != Keys.Control) {
					inventory.Remove(hoverItem);
				}
				inventoryImage.DoDragDrop(hoverItem.Clone(), DragDropEffects.Move);
			}
		}

		private void inventoryImage_MouseEnter(object sender, EventArgs e) {
			itemDetailLabel.Visible = true;
		}

		private void inventoryImage_MouseLeave(object sender, EventArgs e) {
			itemDetailLabel.Visible = false;
		}

		private InventoryItem getItemFromPoint(int x, int y) {
			return inventory.LastOrDefault(i => x >= i.X && x < i.X + i.Item.Width && y >= i.Y && y < i.Y + i.Item.Height);
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e) {
			if (!promptToSave()) { return; }
			fileName = null;
			inventory.Clear();
			inventoryImage.Invalidate();
		}

		private bool promptToSave() {
			if (dirtyFlag) {
				var response = MessageBox.Show("Save changes?", this.Text, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (response == System.Windows.Forms.DialogResult.Cancel) {
					return false;
				}
				if (response == System.Windows.Forms.DialogResult.Yes) {
					return saveFile();
				}
				return true;
			} else {
				return true;
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
			if (!promptToSave()) {
				e.Cancel = true;
			}
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
			this.Close();
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
			new AboutBox().ShowDialog();
		}

		private void readmeToolStripMenuItem_Click(object sender, EventArgs e) {
			if (!File.Exists("Readme.txt")) {
				try {
					File.WriteAllText("Readme.txt", Properties.Resources.Readme);
				} catch (Exception) {
					using (Form f = new Form() { Text = "Sir Inventory Editor Readme", Width = 750, Height = 500, AutoScroll = true }) {
						TextBox tb = new TextBox() {
							Text = Properties.Resources.Readme,
							Dock = DockStyle.Fill,
							Multiline = true,
							ReadOnly = true,
							Font = new Font(FontFamily.GenericMonospace, 8),
							ScrollBars = ScrollBars.Both,
							WordWrap = false,
						};
						tb.Select(0, 0);
						f.Controls.Add(tb);
						f.ShowDialog();
					}

					return;
				}
			}

			Process.Start("Readme.txt");
		}

		private void inventoryImage_MouseClick(object sender, MouseEventArgs e) {
			InventoryItem hoverItem = getItemFromPoint(e.X / tileSize, e.Y / tileSize);
			if (e.Button == System.Windows.Forms.MouseButtons.Right && hoverItem != null) {
				CountAdjuster ca = new CountAdjuster() { Max = hoverItem.Item.MaxCount, Count = hoverItem.Count };
				ca.StartPosition = FormStartPosition.Manual;
				Point screenPoint = inventoryImage.PointToScreen(new Point(e.X, e.Y));
				ca.Location = new Point(screenPoint.X - ca.Width / 2, screenPoint.Y - ca.Height / 2);
				if (ca.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
					hoverItem.Count = ca.Count;
				}
			}
		}
	}
}
