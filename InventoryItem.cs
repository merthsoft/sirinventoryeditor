using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace SirInventoryEditor {
	public class InventoryItem {
		public SirItem Item { get; set; }
		public InventoryPoint Location { get; set; }
		public int Count { get; set; }

		public int X {
			get { return Location.X; }
			set { Location.X = value; }
		}

		public int Y {
			get { return Location.Y; }
			set { Location.Y = value; }
		}

		public string Name {
			get {
				return Item.Name;
			}
		}

		public InventoryItem(SirItem item) {
			Item = item;
			Count = item.MaxCount;
			Location = new InventoryPoint();
		}

		public InventoryItem(SirItem item, int x, int y)
			: this(item) {
			Location = new InventoryPoint(x, y);
		}

		public InventoryItem(SirItem item, InventoryPoint point)
			: this(item) {
			Location = point;
		}

		public InventoryItem Clone() {
			return new InventoryItem(Item, X, Y);
		}
	}
}
