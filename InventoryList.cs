﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SirInventoryEditor {
	[XmlRoot("C_InventoryData")]
	public class InventoryList {
		[XmlElement("m_InventoryName", Order = 1)]
		public string InventoryName { get; set; }

		[XmlArray("m_PickupItemList", Order = 2)]
		public List<string> PickupItemList { get; set; }

		[XmlArray("m_PickupItemValueList", Order = 3)]
		public List<int> PickupItemValueList { get; set; }

		[XmlArray("m_PickupPositionList", Order = 4)]
		[XmlArrayItem("S_InvPoint")]
		public List<InventoryPoint> PickupPositionList { get; set; }

		public InventoryList() {
			PickupItemList = new List<string>();
			PickupItemValueList = new List<int>();
			PickupPositionList = new List<InventoryPoint>();
		}
	}

	public class InventoryPoint {
		[XmlElement("x")]
		public int X { get; set; }

		[XmlElement("y")]
		public int Y { get; set; }

		public InventoryPoint() { }

		public InventoryPoint(int x, int y) {
			X = x;
			Y = y;
		}
	}
}
