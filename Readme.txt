﻿Sir Inventory Editor
Merthsoft Creations 2014
Shaun McFall
shaunmm.mcfall@gmail.com

This program can be used to edit the player inventory for "Sir, You Are Being Hunted".

How to use:
You can either start from scratch or open an existing file. Player inventory files are located
at <Sir Directory>\SaveGames\World<world#>\Dynamic_Content\PlayerInv.xml. Once you've started
a new inventory or loaded an existing file, drag and drop items from the left pane into the
inventory. Drag around the inventory to re-arrange items. Ctrl+Drag an item in the inventory to
duplicate an item. Right click to adjust the count of the item (for ammo etc.). Drag out of the
inventory to remove the item.

Note: I don't check on things like overlapping, out-of-bounds, or max-ammo, because I wanted
this tool to be as flexible as possible. You can do things that the game counts as illegal,
and as such the game may not load properly. It's up to you to do it right.

Adding new items:
The item list is driven by two things, the SirItems.xml file, and all the images in the 
Images directory. To add a new item, give it a new item definition in the SirItems.xml file, 
and add a new image to Images with the same name. For example, if you're adding a new item that 
is called "Watermelon" in the save file, which is 2x2, you would add this to SirItems.xml:
<SirItem name="Watermelon" description="A juicy watermelon" width="2" height="2" maxCount="1" />
And in the Images directory, you would add an image called Watermelon.png. It MUST be a .png.

I'm not responsible for anything bad that happens to your save game from using this tool. ALWAYS
backup your inventory before making modifications, especially if there's been a new release of the
game. This was made for alpha v0.7.5095. Do whatever you want with the program or code--it's all
free to do with as you please.