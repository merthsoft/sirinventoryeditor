﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SirInventoryEditor {
	public class SirItem : IComparable<SirItem> {
		public const int TileSize = 64;

		private string name;
		[XmlAttribute("name")]
		public string Name {
			get {
				return name;
			}
			set {
				name = value;
			}
		}

		[XmlAttribute("description")]
		public string Description { get; set; }

		[XmlAttribute("width")]
		public int Width { get; set; }

		[XmlAttribute("height")]
		public int Height { get; set; }

		[XmlAttribute("maxCount")]
		public int MaxCount { get; set; }

		private Image image;
		[XmlIgnore]
		public Image Image {
			get {
				if (image == null) {
					try {
						image = new Bitmap(TileSize*Width, TileSize * Height);
						using (Graphics g = Graphics.FromImage(image)) {
							g.DrawImage(Bitmap.FromFile(string.Format("Images/{0}.png", name)), 0, 0, TileSize * Width, TileSize * Height);
						}						
					} catch {
						//MessageBox.Show(string.Format("Unable to open Images\\{0}.png, please check that it exists in the Images directory.", name), "Error loading file", MessageBoxButtons.OK, MessageBoxIcon.Error);
						image = new Bitmap(TileSize * Width, TileSize * Height);
						using (Font f = new Font(FontFamily.GenericSansSerif, 15))
						using (Graphics g = Graphics.FromImage(image)) {
							g.DrawString(name.SpaceBeforeCapitals(), f, Brushes.Black, new RectangleF(0, 0, TileSize*Width, TileSize * Height));
						}
					}
				}
				return image;
			}
		}

		private Image thumbnail;
		[XmlIgnore]
		public Image Thumbnail {
			get {
				if (thumbnail == null) {
					thumbnail = new Bitmap(TileSize, TileSize);
					using (Graphics g = Graphics.FromImage(thumbnail)) {
						if (Width == Height) {
							g.DrawImage(Image, 0, 0, TileSize, TileSize);
						} else if (Width > Height) {
							g.DrawImage(Image, 0, 0, TileSize, TileSize / (Width / Height));
						} else if (Height > Width) {
							g.DrawImage(Image, 0, 0, TileSize / (Height / Width), TileSize);
						}
					}
				}

				return thumbnail;
			}
		}

		public override string ToString() {
			return string.Format("{0} ({1}x{2})", Name, Width, Height);
		}

		public int CompareTo(SirItem other) {
			return Name.CompareTo(other.Name);
		}
	}
}
